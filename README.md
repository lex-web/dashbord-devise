** TABLEAU DE BORD - DEVISE **

Différents graphiques dynamique sur la monaie mondial

** Développement**
    - Utilisation de l'API "https://api.exchangerate.host" (Données gratuite)
    - Développement de l'api en Python
    - Développement de l'application web avec DJANGO


** Intallation **
Faire un git clone du projet

Puis lancer cette commande :
    pip install -r requirements.txt

Lancement du serveur:
    python manage.py runserver
